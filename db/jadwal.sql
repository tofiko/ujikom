-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 29 Agu 2019 pada 18.10
-- Versi Server: 10.1.28-MariaDB
-- PHP Version: 5.6.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jadwal`
--
CREATE DATABASE IF NOT EXISTS `jadwal` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `jadwal`;

-- --------------------------------------------------------

--
-- Struktur dari tabel `dosen`
--

CREATE TABLE `dosen` (
  `kode_dosen` varchar(5) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `rate` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `dosen`
--

INSERT INTO `dosen` (`kode_dosen`, `nama`, `tanggal_lahir`, `alamat`, `telp`, `rate`) VALUES
('DS001', 'Abdul Hamkah', '1985-04-22', 'Jl. Plumpang Raya No. 66', '085711224422', 25000),
('DS002', 'Iqbal Yahya', '1988-03-11', 'Jl. Medan Barat No. 70', '085244556688', 230000),
('DS003', 'Ali Imran', '1990-01-28', 'Jl. Majapahit No.22', '081313221515', 200000),
('DS004', 'Sunarto', '1983-12-28', 'Jl. Melati Timur No. 99', '088811335533', 220000),
('DS005', 'Alfin Faris', '1991-09-05', 'Jl. Gajah Mada No. 42', '081322667766', 250000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `jadwal`
--

CREATE TABLE `jadwal` (
  `kode_jadwal` varchar(10) NOT NULL,
  `kode_kelas` varchar(10) NOT NULL,
  `kode_dosen` varchar(5) NOT NULL,
  `kode_ruangan` varchar(5) NOT NULL,
  `mata_kuliah` varchar(50) NOT NULL,
  `sks` int(11) NOT NULL,
  `tanggal` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jadwal`
--

INSERT INTO `jadwal` (`kode_jadwal`, `kode_kelas`, `kode_dosen`, `kode_ruangan`, `mata_kuliah`, `sks`, `tanggal`) VALUES
('JD201901', 'KL003', 'DS003', 'RK004', 'Dasar dasar pemograman', 4, '2019-03-27'),
('JD201902', 'KL004', 'DS001', 'RK005', 'Dasar dasar akuntansi', 2, '2019-08-27'),
('JD201903', 'KL001', 'DS004', 'RK001', 'Kewirausahaan', 2, '2019-08-27'),
('JD201904', 'KL003', 'DS005', 'RK002', 'Pemograman web 1', 4, '2019-08-27'),
('JD201905', 'KL005', 'DS003', 'RK003', 'Perpajakan 1', 4, '2019-08-27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelas`
--

CREATE TABLE `kelas` (
  `kode_kelas` varchar(10) NOT NULL,
  `kelas` varchar(10) NOT NULL,
  `jurusan` varchar(30) NOT NULL,
  `angkatan` int(11) NOT NULL,
  `tingkat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kelas`
--

INSERT INTO `kelas` (`kode_kelas`, `kelas`, `jurusan`, `angkatan`, `tingkat`) VALUES
('KL001', 'AP-13-020', 'Administrasi Bisnis', 2016, 2),
('KL002', 'IK-13-001', 'Management Informatika', 2016, 2),
('KL003', 'IK-13-002', 'Management Informatika', 2015, 3),
('KL004', 'KA-13-010', 'Komputer Akuntansi', 2016, 2),
('KL005', 'KA-13-011', 'Komputer Akuntansi', 2015, 3);

-- --------------------------------------------------------

--
-- Struktur dari tabel `ruangan`
--

CREATE TABLE `ruangan` (
  `kode_ruangan` varchar(5) NOT NULL,
  `nama_ruangan` varchar(25) NOT NULL,
  `letak` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ruangan`
--

INSERT INTO `ruangan` (`kode_ruangan`, `nama_ruangan`, `letak`) VALUES
('RK001', 'Galaxy', 'Lantai 3'),
('RK002', 'Jelly Bean', 'Lantai 1'),
('RK003', 'Google', 'Lantai 1'),
('RK004', 'Eclair', 'Lantai 2'),
('RK005', 'Oreo', 'Lantai 3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`kode_dosen`);

--
-- Indexes for table `jadwal`
--
ALTER TABLE `jadwal`
  ADD PRIMARY KEY (`kode_jadwal`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`kode_kelas`);

--
-- Indexes for table `ruangan`
--
ALTER TABLE `ruangan`
  ADD PRIMARY KEY (`kode_ruangan`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
