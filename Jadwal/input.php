<?php
  if(@$_POST['submit']){
    $kode_jadwal = $_POST['kode_jadwal'];
    $kode_kelas = $_POST['kelas'];
    $kode_dosen = $_POST['nama_dosen'];
    $kode_ruangan = $_POST['ruangan'];
    $mata_kuliah = $_POST['mata_kuliah'];
    $sks = $_POST['sks'];
    $tanggal = $_POST['thn']."-".$_POST['bln']."-".$_POST['tgl'];
    
    mysqli_query($cn, "INSERT INTO jadwal 
      VALUES('".$kode_jadwal."','".$kode_kelas."',
      '".$kode_dosen."','".$kode_ruangan."',
      '".$mata_kuliah."',".$sks.",'".$tanggal."')");
    
    header('Location:index.php?page=list_jadwal');
    exit();
  }
?>
<div class="col-md-12">
  <div class="box box-info">
    <!-- Judul -->
    <div class="box-header with-border">
      <h3 class="box-title">Input Jadwal</h3>
    </div>
    <!-- form -->
    <form class="form-horizontal" id="validation-form" method="post" action="index.php?page=input_jadwal">
      <div class="box-body">
        <div class="form-group">
          <label class="control-label col-sm-3" for="kode_jadwal">Kode Jadwal</label>
            <div class="col-sm-6">
              <div class="clearfix">
                <input type="text" class="form-control" name="kode_jadwal" id="kode_jadwal" placeholder="Kode Jadwal ...">
              </div>
            </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-3" for="kelas">Kelas</label>
            <div class="col-sm-6">
              <select id="kelas" name="kelas" class="select2" data-placeholder="Pilih Kelas ...">
                <option value="">&nbsp;</option>
                <?php
                    $kelas = mysqli_query($cn,"SELECT * FROM kelas");
                    while ($data = mysqli_fetch_array($kelas)) {
                  ?>
                  <option value="<?=$data['kode_kelas'];?>"><?=$data['kelas'];?></option>
                  <?php
                    }
                  ?>
              </select>
            </div>
        </div>
        <div class="form-group row">
          <label class="control-label col-sm-3">Tanggal</label>
            <div class="col-sm-2">
              <select class="form-control" name="tgl">
                <option value="0">-- Pilih Tanggal --</option>
                  <?php
                    for($tgl=1;$tgl<=31;$tgl++){
                  ?>
                    <option value="<?=$tgl;?>"><?=$tgl;?></option>
                  <?php
                    }
                  ?>
              </select>
            </div>
            <div class="col-sm-2">
              <select class="form-control" name="bln">
                <option value="0">-- Pilih Bulan --</option>
                  <?php
                    $txt_bln = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober',
                                    'November','Desember');
                    for($bln=0;$bln<12;$bln++){
                  ?>
                    <option value="<?=$bln+1;?>">
                      <?=$txt_bln[$bln];?>
                    </option>
                  <?php
                    }
                  ?>
                </select>
            </div>
            <div class="col-sm-4">
              <input type="text" class="form-control" name="thn" id="thn" placeholder="Tahun ...">
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-3" for="dosen">Dosen</label>
            <div class="col-sm-6">
              <select id="dosen" name="nama_dosen" class="select2" data-placeholder="Pilih Dosen ...">
                <option value="">&nbsp;</option>
                <?php
                    $dosen = mysqli_query($cn,"SELECT * FROM dosen");
                    while ($data = mysqli_fetch_array($dosen)) {
                  ?>
                  <option value="<?=$data['kode_dosen'];?>"><?=$data['nama'];?></option>
                  <?php
                    }
                  ?>
              </select>
            </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-3" for="ruangan">Ruangan</label>
            <div class="col-sm-6">
              <select id="ruangan" name="ruangan" class="select2" data-placeholder="Pilih Ruangan ...">
                <option value="">&nbsp;</option>
                <?php
                    $ruangan = mysqli_query($cn,"SELECT * FROM ruangan");
                    while ($data = mysqli_fetch_array($ruangan)) {
                  ?>
                    <option value="<?=$data['kode_ruangan'];?>"><?=$data['nama_ruangan'];?></option>
                  <?php
                    }
                  ?>
              </select>
            </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-3" for="mata_kuliah">Mata Kuliah</label>
            <div class="col-sm-6">
              <div class="clearfix">
                <input type="text" class="form-control" name="mata_kuliah" id="mata_kuliah" placeholder="Mata Kuliah ...">
              </div>
            </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-3" for="sks">SKS</label>
            <div class="col-sm-6">
              <div class="clearfix">
                <input type="text" class="form-control" name="sks" id="sks" placeholder="SKS ...">
              </div>
            </div>
        </div>
      <!-- Btn -->
      <div class="box-footer">
        <button type="submit" name="submit" class="btn btn-info center-block" value="Submit">Save</button>
      </div>
    </form>
  </div>
</div>