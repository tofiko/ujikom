<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <!-- Btn Tambah -->
            <div class="box-header">
              <div class="col-md-2" style="padding: 0;">
                <a href="index.php?page=input_jadwal">
                  <button class="form-control btn btn-primary" data-toggle="modal" data-target="#inputperalatan">
                    <i class="glyphicon glyphicon-plus-sign"></i> Tambah Data
                  </button>
                </a>
              </div>
            </div>
            <!-- Judul -->
            <div class="box-header">
              <h3 class="box-title">Data Jadwal</h3>
            </div>
            <div class="box-body">
              <table id="jadwal" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Kode Jadwal</th>
                  <th>Tanggal</th>
                  <th>Kelas</th>
                  <th>Jurusan</th>
                  <th>Matakuliah</th>
                  <th>SKS</th>
                  <th>Ruangan</th>
                  <th>Letak</th>
                  <th>Dosen</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                <?php
                  $qr = mysqli_query($cn,
                        "select * from jadwal as JD
                        INNER JOIN kelas AS KL ON JD.kode_kelas=KL.kode_kelas
                        INNER JOIN ruangan AS R ON JD.kode_ruangan=R.kode_ruangan
                        INNER JOIN dosen AS D ON JD.kode_dosen=D.kode_dosen
                        ORDER BY kode_jadwal ASC");
                while ($data = mysqli_fetch_array($qr)) {
                ?>
                <tr>
                  <td><?=$data['kode_jadwal'];?></td>
                  <td><?=$data['tanggal'];?></td>
                  <td><?=$data['kelas'];?></td>
                  <td><?=$data['jurusan'];?></td>
                  <td><?=$data['mata_kuliah'];?></td>
                  <td><?=$data['sks'];?></td>
                  <td><?=$data['nama_ruangan'];?></td>
                  <td><?=$data['letak'];?></td>
                  <td><?=$data['nama'];?></td>
                  <td class="text-center" style="min-width:86px;">
                    <a href="index.php?page=edit_jadwal&kode=<?=$data['kode_jadwal'];?>">
                      <button class="btn btn-primary"><i class="fa fa-pencil"></i></button>
                    </a>
                    <a href="Jadwal/delete.php?kode=<?=$data['kode_jadwal'];?>" 
                      onclick="return confirm('Yakin Ingin hapus Data?');">
                      <button class="btn btn-danger"><i class="fa fa-trash-o"></i></button> 
                    </a>
                  </td>
                </tr>
                <?php
                  }
                ?>
                </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>