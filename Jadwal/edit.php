<?php
  if(@$_POST['submit']){
    $kode_jadwal = $_POST['kode_jadwal'];
    $kode_kelas = $_POST['kelas'];
    $kode_dosen = $_POST['nama_dosen'];
    $kode_ruangan = $_POST['ruangan'];
    $mata_kuliah = $_POST['mata_kuliah'];
    $sks = $_POST['sks'];
    $tanggal = $_POST['thn']."-".$_POST['bln']."-".$_POST['tgl'];
    
    mysqli_query($cn,"UPDATE jadwal SET kode_kelas='".$kode_kelas."',
    kode_dosen='".$kode_dosen."',kode_ruangan='".$kode_ruangan."',
    mata_kuliah='".$mata_kuliah."',sks=".$sks.",
    tanggal='".$tanggal."' WHERE kode_jadwal='".$kode_jadwal."'");
    
    header('Location:index.php?page=list_jadwal');
    exit();
  }
  
  $edit_jadwal = @$_GET['kode'];
  $qr = mysqli_query($cn,"SELECT * FROM jadwal as jd 
      INNER JOIN kelas as kl ON jd.kode_kelas=kl.kode_kelas 
      INNER JOIN ruangan as rk ON jd.kode_ruangan=rk.kode_ruangan 
      INNER JOIN dosen as ds ON jd.kode_dosen=ds.kode_dosen 
      WHERE kode_jadwal='".$edit_jadwal."'");
  while($data = mysqli_fetch_array($qr)){
    $edit_kelas = $data['kode_kelas'];
    $edit_ruangan = $data['kode_ruangan'];
    $edit_dosen = $data['kode_dosen'];
    $edit_mata_kuliah = $data['mata_kuliah'];
    $edit_sks = $data['sks'];
    $edit_tgl = explode("-",$data['tanggal']);
  }
?>
<div class="col-md-12">
  <div class="box box-info">
    <!-- Judul -->
    <div class="box-header with-border">
      <h3 class="box-title">Input Jadwal</h3>
    </div>
    <!-- form -->
    <form class="form-horizontal" id="validation-form" method="post" action="index.php?page=edit_jadwal">
      <div class="box-body">
        <div class="form-group">
          <label class="control-label col-sm-3" for="kode_jadwal">Kode Jadwal</label>
            <div class="col-sm-6">
              <div class="clearfix">
                <input type="text" class="form-control" name="kode_jadwal" value="<?=$edit_jadwal;?>" readonly>
              </div>
            </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-3" for="kelas">Kelas</label>
            <div class="col-sm-6">
              <select id="kelas" name="kelas" class="select2" data-placeholder="Pilih Kelas ...">
                <option value="">&nbsp;</option>
                  <?php
                    $kelas = mysqli_query($cn,"SELECT * FROM kelas");
                    while ($data = mysqli_fetch_array($kelas)) {
                      if($edit_kelas == $data['kode_kelas']){
                        $slc_kls = 'selected';}
                      else{$slc_kls = '';}
                  ?>
                    <option <?=$slc_kls;?> value="<?=$data['kode_kelas']?>"><?=$data['kelas'];?></option>
                  <?php
                    }
                  ?>
              </select>
            </div>
        </div>
        <div class="form-group row">
          <label class="control-label col-sm-3">Tanggal</label>
            <div class="col-sm-2">
              <select class="form-control" name="tgl">
                <option value="0">-- Pilih Tanggal --</option>
                  <?php
                    for($tgl=1;$tgl<=31;$tgl++){
                      if($edit_tgl[2] == $tgl){$slc_tgl = 'selected';}
                      else{$slc_tgl = '';}
                  ?>
                    <option <?=$slc_tgl;?> value="<?=$tgl;?>"><?=$tgl;?></option>
                  <?php
                    }
                  ?>
              </select>
            </div>
            <div class="col-sm-2">
              <select class="form-control" name="bln">
                <option value="0">-- Pilih Bulan --</option>
                  <?php
                    $txt_bln = array('Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober',
                                    'November','Desember');
                    for($bln=0;$bln<12;$bln++){
                      if($edit_tgl[1] == $bln){$slc_bln = 'selected';}
                      else{$slc_bln = '';}
                  ?>
                    <option <?=$slc_bln;?> value="<?=$bln+1;?>">
                      <?=$txt_bln[$bln];?>
                    </option>
                  <?php
                    }
                  ?>
                </select>
            </div>
            <div class="col-sm-4">
              <input type="text" class="form-control" name="thn" id="thn" value="<?=$edit_tgl[0];?>" placeholder="Tahun ...">
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-3" for="dosen">Dosen</label>
            <div class="col-sm-6">
              <select id="dosen" name="nama_dosen" class="select2" data-placeholder="Pilih Dosen ...">
                <option value="">&nbsp;</option>
                  <?php
                    $dosen = mysqli_query($cn,"SELECT * FROM dosen");
                    while ($data = mysqli_fetch_array($dosen)) {
                      if($edit_dosen == $data['kode_dosen']){
                        $slc_dsn = 'selected';}
                      else{$slc_dsn = '';}
                  ?>
                    <option <?=$slc_dsn;?> value="<?=$data['kode_dosen']?>"><?=$data['nama'];?></option>
                  <?php
                    }
                  ?>
              </select>
            </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-3" for="ruangan">Ruangan</label>
            <div class="col-sm-6">
              <select id="ruangan" name="ruangan" class="select2" data-placeholder="Pilih Ruangan ...">
                <option value="">&nbsp;</option>
                  <?php
                    $ruangan = mysqli_query($cn,"SELECT * FROM ruangan");
                    while ($data = mysqli_fetch_array($ruangan)) {
                      if($edit_ruangan == $data['kode_ruangan']){
                        $slc_rgn = 'selected';}
                      else{$slc_rgn = '';}
                  ?>
                    <option <?=$slc_rgn;?> value="<?=$data['kode_ruangan']?>"><?=$data['nama_ruangan'];?></option>
                  <?php
                    }
                  ?>
              </select>
            </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-3" for="mata_kuliah">Mata Kuliah</label>
            <div class="col-sm-6">
              <div class="clearfix">
                <input type="text" class="form-control" name="mata_kuliah" id="mata_kuliah" value="<?=$edit_mata_kuliah;?>"
                  placeholder="Mata Kuliah ...">
              </div>
            </div>
        </div>
        <div class="form-group">
          <label class="control-label col-sm-3" for="sks">SKS</label>
            <div class="col-sm-6">
              <div class="clearfix">
                <input type="text" class="form-control" name="sks" id="sks" value="<?=$edit_sks;?>" placeholder="SKS ...">
              </div>
            </div>
        </div>
      <!-- Btn -->
      <div class="box-footer">
        <button type="submit" name="submit" class="btn btn-info center-block" value="Submit">Save</button>
      </div>
    </form>
  </div>
</div>